const TimeSeries = "Time Series (Digital Currency Daily)";
const MetaData = "Meta Data";

class Lavadora {
    constructor(currencyDatas) {
        this.currency = {}
        this.currencyData = currencyDatas
        this.getCurrentInfo()
        this.getHistoricalPrices()
    }

    getCurrentInfo() {
        const date = this.currencyData[MetaData]["6. Last Refreshed"].slice(0, 10)

        this.currency.name = this.currencyData[MetaData]["3. Digital Currency Name"];
        this.currency.price = this.currencyData[TimeSeries][date]["4b. close (USD)"];
        this.currency.volume = this.currencyData[TimeSeries][date]["5. volume"];
        this.currency.market = this.currencyData[TimeSeries][date]["6. market cap (USD)"];
        this.currency.date = date
    }
    getHistoricalPrices() {
        const currencyTimeSeries = Object.keys(this.currencyData[TimeSeries])
        const currencySeries = []
        currencyTimeSeries.forEach(element => currencySeries.push(Number(this.currencyData[TimeSeries][element]["4b. close (USD)"])))
    
        this.currency.series = currencySeries
        this.currency.dates = currencyTimeSeries
    }

    getCurrency() {
        return this.currency
    }
}

export default Lavadora