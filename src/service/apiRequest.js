export default async () => {
    const resBitcoin = await fetch('https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_DAILY&symbol=BTC&market=CNY&apikey=81W9UE76FN2USJAF');
    const resEthereum = await fetch('https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_DAILY&symbol=ETH&market=CNY&apikey=81W9UE76FN2USJAF');
    const resRipple = await fetch('https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_DAILY&symbol=XRP&market=CNY&apikey=81W9UE76FN2USJAF');

    const bitcoin = await resBitcoin.json();
    const ethereum = await resEthereum.json();
    const ripple = await resRipple.json();

    return [bitcoin, ethereum, ripple]
}


